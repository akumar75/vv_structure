from pyspark.sql import SparkSession

import os
import sys
import ConfigParser


class Driver_Process(object):
    def __init__(self, spark_session):
        self.spark_session = spark_session

    def get_driver_process_data(self):
        print("Loading Driver Process data")
        df = self.spark_session.read.format("csv").option("header", "true").load("../test/driver_process.csv")
        # sele = self.spark_session.sql("select VariableName,Value from vv_db.processdriver")
        df.createOrReplaceTempView("Driver")
        select_query_op = self.spark_session.sql("select VariableName,Value from Driver")
        out = select_query_op.collect()
        dic = {}
        m = map(lambda x: (x[0], x[1]), out)
        dic = dict(m)
        return dic

    def get_config_properties_value(self):
        config = ConfigParser.RawConfigParser()
        config.read("Python-Jenkins/test/ConfigFile.properties")
        return config
