from src.com.virginvoyages.rec_engine.tribe.tribe import get_driver_process_data
from pyspark.sql import SparkSession
import pytest
import decimal
import numbers


@pytest.fixture(scope="session")
def spark_context(request):
    spark = SparkSession.builder.appName("Python").getOrCreate()
    global driver_process_dictionary
    driver_process_dictionary = get_driver_process_data(spark)
    request.addfinalizer(lambda: spark.stop())
    return spark


pytestmark = pytest.mark.usefixtures("spark_context")


# 1 Total Rows in Table
def test_table_count_is_16(spark_context):
    queryTable = driver_process_dictionary.count()
    assert queryTable == 16


# 2 qryTable
def test_query_table_value(spark_context):
    queryTable = driver_process_dictionary.where("VariableName='qryTable'").select("Value").collect()[0][0]
    assert queryTable == "vv_db.customer_data_text"


def test_query_table_type(spark_context):
    queryTable = driver_process_dictionary.where("VariableName='qryTable'").select("Value").collect()[0][0]
    assert isinstance(queryTable, basestring)


# 3 distillTable


def test_distill_table_value(spark_context):
    queryTable = driver_process_dictionary.where("VariableName='distillTable'").select("Value").collect()[0][0]
    assert queryTable == "ds_db.tribesegdata"


def test_distill_table_type(spark_context):
    queryTable = driver_process_dictionary.where("VariableName='distillTable'").select("Value").collect()[0][0]
    assert isinstance(queryTable, basestring)


# 4 densityLow

def test_densityLow_value(spark_context):
    queryTable = driver_process_dictionary.where("VariableName='densityLow'").select("Value").collect()[0][0]
    assert queryTable == "0.1"


def test_densityLow_type(spark_context):
    density_low = driver_process_dictionary.where("VariableName='densityLow'").select("Value").collect()[0][0]
    assert isinstance(float(density_low), float)


# 5 densityHigh
def test_densityHigh_value(spark_context):
    queryTable = driver_process_dictionary.where("VariableName='densityHigh'").select("Value").collect()[0][0]
    assert queryTable == "0.9"


def test_densityHigh_type(spark_context):
    seedVar = driver_process_dictionary.where("VariableName='densityHigh'").select("Value").collect()[0][0]
    assert isinstance(float(seedVar), float)


# 6 seedVar
def test_seedVar_value(spark_context):
    queryTable = driver_process_dictionary.where("VariableName='seedVar'").select("Value").collect()[0][0]
    assert queryTable == "great_outdoors~luxury_life"


def test_seedVar_type(spark_context):
    seedVar = driver_process_dictionary.where("VariableName='seedVar'").select("Value").collect()[0][0]
    assert isinstance(seedVar, basestring)


# 7 numVarToSelect
def test_numVarToSelect_value(spark_context):
    numVarToSelect = driver_process_dictionary.where("VariableName='numVarToSelect'").select("Value").collect()[0][0]
    assert numVarToSelect == "8"


def test_numVarToSelect_type(spark_context):
    numVarToSelect = driver_process_dictionary.where("VariableName='numVarToSelect'").select("Value").collect()[0][0]
    assert isinstance(int(numVarToSelect), int)


# 8 seedVar
def test_numOfClusters_value(spark_context):
    numOfClusters = driver_process_dictionary.where("VariableName='numOfClusters'").select("Value").collect()[0][0]
    assert numOfClusters == "5"


def test_numOfClusters_type(spark_context):
    numOfClusters = driver_process_dictionary.where("VariableName='numOfClusters'").select("Value").collect()[0][0]
    assert isinstance(int(numOfClusters), int)


# 9 kmeansMaxIter
def test_kmeansMaxIter_value(spark_context):
    kmeansMaxIter = driver_process_dictionary.where("VariableName='kmeansMaxIter'").select("Value").collect()[0][0]
    assert kmeansMaxIter == "10"


def test_kmeansMaxIter_type(spark_context):
    kmeansMaxIter = driver_process_dictionary.where("VariableName='kmeansMaxIter'").select("Value").collect()[0][0]
    assert isinstance(int(kmeansMaxIter), int)


# 10 kmeansSeed
def test_kmeansSeed_value(spark_context):
    kmeansSeed = driver_process_dictionary.where("VariableName='kmeansSeed'").select("Value").collect()[0][0]
    assert kmeansSeed == "3"


def test_kmeansSeed_type(spark_context):
    kmeansSeed = driver_process_dictionary.where("VariableName='kmeansSeed'").select("Value").collect()[0][0]
    assert isinstance(int(kmeansSeed), int)


# 11 gIncome
def test_gIncome_value(spark_context):
    gIncome = driver_process_dictionary.where("VariableName='gIncome'").select("Value").collect()[0][0]
    assert gIncome.find("null")


def test_gIncome_type(spark_context):
    gIncome = driver_process_dictionary.where("VariableName='gIncome'").select("Value").collect()[0][0]
    assert isinstance(gIncome, basestring)


# 12 ageLow
def test_ageLow_value(spark_context):
    ageLow = driver_process_dictionary.where("VariableName='ageLow'").select("Value").collect()[0][0]
    assert ageLow == "20"


def test_ageLow_type(spark_context):
    ageLow = driver_process_dictionary.where("VariableName='ageLow'").select("Value").collect()[0][0]
    assert isinstance(int(ageLow), int)


# 13 ageHigh
def test_ageHigh_value(spark_context):
    ageHigh = driver_process_dictionary.where("VariableName='ageHigh'").select("Value").collect()[0][0]
    assert ageHigh == "45"


def test_ageHigh_type(spark_context):
    ageHigh = driver_process_dictionary.where("VariableName='ageHigh'").select("Value").collect()[0][0]
    assert isinstance(int(ageHigh), int)
