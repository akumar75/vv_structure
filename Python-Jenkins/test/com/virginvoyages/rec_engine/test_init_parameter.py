import pytest
from pyspark.sql import SparkSession

from query_driver_process import Driver_Process


@pytest.fixture(scope="session")
def spark_context(request):
    spark = SparkSession.builder.appName("Python").getOrCreate()
    global driver_process_dictionary, config_properties_value
    driver_process_object = Driver_Process(spark)
    driver_process_dictionary = driver_process_object.get_driver_process_data()
    config_properties_value = driver_process_object.get_config_properties_value()
    request.addfinalizer(lambda: spark.stop())
    return spark


pytestmark = pytest.mark.usefixtures("spark_context")


# 1 Total Rows in Table
def test_table_count_is_14():
    assert len(driver_process_dictionary) == 14


# 2 qryTable
def test_query_table_value():
    assert driver_process_dictionary.get("qryTable") == config_properties_value.get("TribeSegmentation", "qryTable")


def test_query_table_type():
    assert isinstance(driver_process_dictionary.get("qryTable"), basestring)


#
# # 3 distillTable
#
#
def test_distill_table_value():
    assert driver_process_dictionary.get("distillTable") == config_properties_value.get("TribeSegmentation",
                                                                                        "distillTable")


#
#
def test_distill_table_type():
    assert isinstance(driver_process_dictionary.get("distillTable"), basestring)


#
#
# 4 densityLow
#
def test_densityLow_value():
    assert driver_process_dictionary.get("densityLow") == config_properties_value.get("TribeSegmentation", "densityLow")


#
#
def test_densityLow_type():
    assert isinstance(float(driver_process_dictionary.get("densityLow")), float)


#
#
# # 5 densityHigh
def test_densityHigh_value():
    assert driver_process_dictionary.get("densityHigh") == config_properties_value.get("TribeSegmentation",
                                                                                       "densityHigh")


#
#
def test_densityHigh_type():
    assert isinstance(float(driver_process_dictionary.get("densityHigh")), float)


#
#
# # 6 seedVar
def test_seedVar_value():
    assert driver_process_dictionary.get("seedVar") == config_properties_value.get("TribeSegmentation",
                                                                                   "seedVar")


#
#
def test_seedVar_type():
    assert isinstance(driver_process_dictionary.get("seedVar"), basestring)


#
#
# # 7 numVarToSelect
def test_numVarToSelect_value():
    assert driver_process_dictionary.get("numVarToSelect") == config_properties_value.get("TribeSegmentation",
                                                                                          "numVarToSelect")


#
#
def test_numVarToSelect_type():
    assert isinstance(int(driver_process_dictionary.get("numVarToSelect")), int)


#
#
# # 8 seedVar
def test_numOfClusters_value():
    assert driver_process_dictionary.get("numOfClusters") == config_properties_value.get("TribeSegmentation",
                                                                                         "numOfClusters")


#
#
def test_numOfClusters_type():
    assert isinstance(int(driver_process_dictionary.get("numOfClusters")), int)


#
#
# # 9 kmeansMaxIter
def test_kmeansMaxIter_value():
    assert driver_process_dictionary.get("kmeansMaxIter") == config_properties_value.get("TribeSegmentation",
                                                                                         "kmeansMaxIter")


#
#
def test_kmeansMaxIter_type():
    assert isinstance(int(driver_process_dictionary.get("kmeansMaxIter")), int)


#
#
# # 10 kmeansSeed
def test_kmeansSeed_value():
    assert driver_process_dictionary.get("kmeansSeed") == config_properties_value.get("TribeSegmentation",
                                                                                      "kmeansSeed")


#
#
def test_kmeansSeed_type():
    assert isinstance(int(driver_process_dictionary.get("kmeansSeed")), int)


#
#
# # 11 gIncome
def test_gIncome_value():
    assert driver_process_dictionary.get("gIncome") == config_properties_value.get("TribeSegmentation",
                                                                                   "gIncome")


#
#
def test_gIncome_type():
    assert isinstance(driver_process_dictionary.get("gIncome"), basestring)


#
#
# # 12 ageLow
def test_ageLow_value():
    assert driver_process_dictionary.get("ageLow") == config_properties_value.get("TribeSegmentation",
                                                                                  "ageLow")


#
#
def test_ageLow_type():
    assert isinstance(int(driver_process_dictionary.get("ageLow")), int)


#
# # 13 ageHigh
def test_ageHigh_value():
    assert driver_process_dictionary.get("ageHigh") == config_properties_value.get("TribeSegmentation",
                                                                                   "ageHigh")


#
#
def test_ageHigh_type():
    assert isinstance(int(driver_process_dictionary.get("ageHigh")), int)
