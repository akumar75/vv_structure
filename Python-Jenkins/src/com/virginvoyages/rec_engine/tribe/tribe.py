from pyspark.sql import SparkSession

import os
import sys
import ConfigParser



def get_driver_process_data(spark_context):
    print("Loading Driver Process data")
    df = spark_context.read.format("csv").option("header", "true").load("Python-Jenkins/test/driver_process.csv")
    df.createOrReplaceTempView("Driver")
    sele = spark_context.sql("select VariableName,Value from Driver")
    out = sele.collect()
    dic = {}
    m = map(lambda x: (x[0], x[1]), out)
    dic = dict(m)
    return df


def get_config_properties_value():
    config = ConfigParser.RawConfigParser()
    config.read("test/ConfigFile.properties")
    return config


if __name__ == "__main__":
    config = ConfigParser.RawConfigParser()
    config.read("Python-Jenkins/test/ConfigFile.properties")
    print config.get("TribeSegmentation", "selectedVar")
